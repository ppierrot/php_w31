<?php
  if(isset($_SESSION['login'])){
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Mon compte</title>
    </head>
    <body class="container">
      <h1 class="center-align">Bienvenue, <?= $_SESSION['login']; ?> !</h1>
      <a href="/account/formpassword">Changer le mot de passe</a>
      <a href="/account/deleteuser">Supprimer compte</a>
      <a href="/account/listecourses">Listes de course</a>
      <a href="/account/signout">Deconnexion</a>
    </body>
</html>

<?php
  }
?>