<?php

  namespace App\Http\Controllers;

  use App\Models\Course;
  use App\Models\Course_Article;
  use App\Models\Model_base;
  use PDO;
  use Illuminate\Http\Request;


  class CourseController extends Controller
  {
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }


    //addList
    //modifList
    //addArticle
    //articleStat

    public function addList(Request $request)
    {

      //si pas login utilisateur ou pas de nom en post
      if(!isset($_SESSION['login']) || !$request->has('name_list')){
        echo'login ou nom de liste de courses non renseigné';
      }

      //protège l'intégrité des balises html enregistrées
      $login = htmlentities($_SESSION['login']);
      $nameList = htmlentities($request->input('name_list'));

      //création objet list
      $obj = new Course($login, $nameList);

      $obj->create();
    
      redirect('/account/listecourses');
    }

    /*transfert les noms des listes de courses détenus par un utilisateur dans variable de session*/
    public function recuperer(Request $request){

      if(!isset($_SESSION['login'])){
        echo'nom utilisateur non renseigné'; 
      }

      //protège l'intégrité des balises html enregistrées
      $login = htmlentities($_SESSION['login']);

      $obj = new Course($login, "");

      $obj->recup();

      return view('listes_courses');
    }


    //récupérer nom d'utilisateur
    //modifList
    //addArticle
    //articleStat
  }

?>