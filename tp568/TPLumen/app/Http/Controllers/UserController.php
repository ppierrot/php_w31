<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Model_base;
use PDO;
use Illuminate\Http\Request;


class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function adduser (Request $request)
    {

        // si requête passé en post, si id, mdp ou conf_mdp existent pas
        if(!$request->has('id') || !$request->has('mdp') || !$request->has('conf_mdp')){
            return redirect('/signup');
        }

        //si mot de passe =/= confirmation de mot de passe
        if(strcmp($request->input('mdp'), $request->input('conf_mdp')) !== 0){
            return redirect('/signup');
        }

        //protège l'intégrité des balises html enregistrées
        $login = htmlentities($request->input('id'));
        $password = htmlentities($request->input('mdp'));

        //création objet user
        $obj = new User($login, $password);

        $obj->create();
    
        return view('/signin');
    }

    public function deleteuser(Request $request)
    {
  
        if(isset($_SESSION['login'])){
            //création objet user
            $obj = new user($_SESSION['login'], "");

            $obj->delete();

            // on détruit la session en cours
            session_destroy();

            return view('signin');
        }
    }

    public function changepassword(Request $request)
    {
  
        //si utilisateur est connécté et si requêtes de type post
        if(isset($_SESSION['login'])){

            //si variables dans requête POST existent
            if(!$request->has('new_mdp') || !$request->has('conf_new_mdp')){
                redirect('/account/formpassword');
            }

            //si mot passe = confirmation mot de passe
            if($request->input('new_mdp') === $request->input('conf_new_mdp')){

                //protège l'intégrité des balises html enregistrées
                $login = htmlentities($_SESSION['login']);
                $new_mdp = htmlentities($request->input('new_mdp'));

                //création objet user
                $obj = new user($login, $new_mdp);

                $obj->changePassword($new_mdp);

                return redirect('/account/welcome');
            }
            else{
                return redirect('/account/formpassword');
            }
        } 
        else {
            return redirect('/account/formpassword');
        }
    }

    public function signout()
    {
        // on dÃ©truit la variable de session 'login'
        unset($_SESSION['login']);
        // et on envoie une demande de redirection en GET vers signin.php
        return view('signin');
    }

    public function authenticate(Request $request)
    {

        //si requêtes post du login et password existent
        if($request->has('login') && $request->has('password')){

            //protège l'intégrité des balises html enregistrées
            $login = htmlentities($request->input('login'));
            $password = htmlentities($request->input('password'));

            //création objet user
            $obj = new user($login, $password);

            //si le résultat retourné par l'appel de la fonction exists est true
            if($obj->exists() == true){
                $_SESSION['login'] = $login;
                return redirect('account/welcome');
            }
            else{
                return redirect('signin');
            }
        }
        else{
            return redirect('signin');
        }
    }

    //prend en paramètre une route, genre le pseudo de l'utilisateur inclut dans l'url, et met à jour cette même url
    //public function update(Request $request, $id)
    //{

    //}
    //
}