<?php

  namespace App\Models;

  use PDO;

  class Course_Article extends Model_base{

    /*membres*/

    private $_name;
    private $_state;
    private $_listId;
    const ARTICLE_TABLE = "list_elmt";

    /*constructeur de la classe*/

    function __construct(string $name, int $listId) { 
      
      $this->_name = $name; 
      $this->_state = FALSE; //false = à acheter, true = dans caddit
      $this->_listId = $listId;
    }

    /*getters*/

    function getName(){ 
      return $this->_name; 
    }

    function getState(){
      return $this->_state;
    }

    function getlistId(){
      return $this->_listId;
    }

    /*setters*/

    function setName(string $name){ 
      $this->_name = $name; 
    }

    function setState(bool $state){
      $this->_state = $state;
    }

    function setProprio(int $listId){
      $this->_listId = $listId;
    }

    /*méthodes*/
  
    /*ajouter un article à la liste*/
    public function newArticle() {
      
      
    }

    /*affiche le nom des articles de la liste*/
    public function afficher(){

    }

    /*interchange l'état d'un article*/
    public function changeState() {

    }

  }

?>