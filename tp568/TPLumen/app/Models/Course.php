<?php

  namespace App\Models;

  use PDO;
  use App\Models\List_Article;

  class Course extends Model_base{

    /*membres*/

    private $_userId;
    private $_name;
    const COURSE_TABLE = "list_courses";

    /*constructeur de la classe*/

    function __construct(string $userId, string $name) { 
      
      $this->_userId = $userId; 
      $this->_name = $name;
    
    }

    /*getters*/

    function getUserId(){ 
      return $this->_userId; 
    }

    function getName(){ 
      return $this->_name; 
    }

    /*setters*/

    function setUserId(string $userId){ 
      $this->_userId = $userId; 
    }

    function setName(string $name){ 
      $this->_name = $name; 
    }

    /*méthodes*/
  
    /*Créé un nouvelle liste dans la bdd*/
    public function create() {

      $result = model_base::$_db->prepare("INSERT INTO " . course::COURSE_TABLE . " (nom, proprietaire) values (:name, :userId)");

      $ok1 = $result->bindValue( ':name', $this->_name, PDO::PARAM_STR); 
      $ok2 = $result->bindValue( ':userId', $this->_userId, PDO::PARAM_STR); 

      echo "test";
      //ça bug à partir d'ici

      try{
        $ok3 = $result->execute();
      } catch(Exception $e){
        echo $e->getMessage();
      }

      return redirect("/account/listecourses");

    }

    public function recup(){
      //test
      echo $this->_userId;

      $result = model_base::$_db->prepare("SELECT nom FROM " . course::COURSE_TABLE . " WHERE proprietaire = :userId");
      
      $ok1 = $result->bindValue( ':userId', $this->_userId, PDO::PARAM_STR); 
      
      try{
        $ok2 = $result->execute();
      } catch(Exception $e){
        echo $e->getMessage();
      }
      
      //test
      /*if(empty($result))
      {
        echo "vide";
      }
      else
      {
        echo "pas vide";
      }

      //test affichage, ne sort rien !
      while ($row=$result->fetch(PDO::FETCH_ASSOC)){ 

        echo $row["nom"]; 

      }
      */
      //return view('listes_courses');
    }

    /*renomme une liste*/
    public function renommer(){

    }

    /*supprimer une liste*/
    public function delete(){


    }

  }

?>