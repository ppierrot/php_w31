TP6 - Controller et Middleware
==============================

Dans le TP précédent, nous avons mis en place notre application dans le
framework Lumen en utilisant uniquement un modèle et des vues.

Dans ce TP6, l'objectif est de déplacer toute la logique de contrôle de notre
application (comme par exemple `signin.php`) dans des objets contrôleurs et
middleware, plus souples et plus faciles à manipuler.



Partie I : Un contrôleur de session
-----------------------------------

### Exercice 1 : Préparation

1. Créez un fichier `UserController.php` dans le répertoire approprié.
2. En vous basant sur `ExempleController`, fournit par Lumen, déclarez la
   classe `UserControler` qui étend `Controller`. Elle doit avoir un
   constructeur vide.

Deux étapes vont maintenant être nécessaires pour transformer en véritable
contrôleur chaque contrôleur actuellement déclarés comme une vue :

a. migrer le code vers une fonction de la classe `UserController`.

b. appeler cette fonction depuis `web.php`.


### Exercice 2 : Création du contrôleur authenticate

#### *a. Migration du code*

Pour cette partie, vous lisez la documentation sur
[les requêtes](https://lumen.laravel.com/docs/5.5/requests), notamment à propos
de l'utilisation des fonction `has` et `input` que vous allez utiliser dans les
trois questions.

1. Dans la classe `UserController`, créez une fonction publique `authenticate`
   qui prend en paramètre un objet `$request` de la classe `Request`.
2. Recopiez l'intégralité du code du fichier `authenticate.php` dans la
   fonction `authenticate`.
3. Dans la fonction `authenticate` :
    - remplacez les demande de redirection qui utilisent la directive `header`
      par des retours de fonctions `redirect` de Lumen.
    - remplacez les manipulation des données POST par des appels aux fonctions
      `has` et `input` de l'objet `$request`.
    - Déplacez les directives `use` en entête de la classe `UserController`.

#### *b. Appel de la fonction authenticate depuis web.php*

Pour cette partie, lisez la documentation sur
[les routes](https://lumen.laravel.com/docs/5.5/routing) pour trouver comment
appeler une fonction d'un contrôleur en second paramètre des fonctions `get` et
`post` de l'objet `$router` dans `web.php`.

1. Modifiez `web.php` pour qu'une requête POST vers la route `authenticate`
   déclenche l'exécution de la fonction `authenticate` de la classe
   `UserController`.
2. Testez.
3. Lorsque tout fonctionne, supprimez le fichier `authenticate.php`.


### Exercice 3 : Les contrôleurs signout, changepassword et deleteuser

Même consigne que pour l'exercice 2 avec chacun des trois fichiers suivant :
- `signout.php` : nouvelle fonction `signout` dans `UserController`
- `changepassword.php`: nouvelle fonction `changePassword` dans `UserController`
- `deleteuser.php` : nouvelle fonction `deleteUser` dans `UserController`.

À noter : le fonction `signout` n'a pas besoin de paramètre.

Testez progressivement chacune des nouvelles fonction de contrôle.
Dès qu'une fonction fonctionne, supprimez la vue correspondante.



Partie II : Un middleware pour les sessions
-------------------------------------------

### Exercice 1 : Se documenter

Un middleware est un contrôleur que l'on va pouvoir faire exécuter soit à
chaque requête vers notre application, soit seulement sur une ou plusieurs
routes choisies.

Pour cette partie, lisez la documentation sur les middleware, notamment les
deux premières sections :
- [Defining Middleware](https://lumen.laravel.com/docs/5.5/middleware#defining-middleware)
  qui explique comment écrire un Middleware
- [Registering Middleware](https://lumen.laravel.com/docs/5.5/middleware#registering-middleware)
  qui détaille comment l'utiliser dans les routes de `web.php`

Lisez également la documentation sur les
[groupes de routes](https://lumen.laravel.com/docs/5.5/routing#route-groups)
qui indiquent comment mettre en place un middleware pour un groupe de
routes.


### Exercice 2 : Création du middleware de session

1. Créez un nouveau fichier `SessionMiddleware.php` dans le répertoire
   approprié.
2. Recopiez-y le contenu du fichier `ExampleMiddleware.php` qui se trouve dans
   le même répertoire.
3. Complétez la fonction `handle` pour qu'elle réalise dans l'ordre les
   opérations suivantes :
   - démarrer une session
   - vider la variable de session `message`
   - instancier un objet PDO à partir des variables d'environnement
   - affecter l'objet PDO à l'instance statique de `Model_Base` (via la
     fonction `set_db`)

Notes :
- Toutes les opérations à ajouter sont déjà présentes dans le contrôleur
`UserController` : il n'y a qu'à faire du copier-coller.
- Basez-vous sur la doc pour que ces opérations aient lieu **avant**
  l'exécution de la requête.


### Exercice 3 : Simplification des contrôleurs

La création du middleware `SessionMiddleware` permet de mutualiser du code
redondant dans toutes nos fonction du contrôleur `UserController`.

1. Dans chacune des fonctions de `UserController`, supprimez le code
   correspondant aux fonctionnalités ajoutées dans la fonction `handle` de
   `SessionMiddleware`.

**Attention** : à ce stade, tout ce qui fait appel à un contrôleur ne doit plus
fonctionner ! Cela va s'arranger progressivement avec l'exercice suivant.


### Exercice 4 : Appel du middleware de session dans les routes

Pour cet exercice, les informations de syntaxe dans la documentation que vous
avez lue à l'exercice 1.

1. Dans le fichier `bootstrap/app.php`, ajoutez `SessionMiddleware` comme
   middleware de route, c'est-à-dire pas global (voir la section "Register
   Middleware" du fichier `app.php`).

2. Dans `web.php`, ajoutez un appel à `SessionMiddleware` lors d'une requête
   POST vers la route `authenticate`. Vérifiez que cela fonctionne en vous
   authentifier à partir de `signin.php`

3. Faites de même pour une requête POST vers la route `adduser`.

4. Faite de même pour le groupe de routes de préfixe `account` (et pas pour
    chacune des routes séparément).

5. Dans les vues `welcome.php` et `formpassword.php`, supprimez la création des
   sessions : elle sera déjà instanciée par le `SessionMiddleware` exécuté lors
   d'une requête vers le préfixe `account`.


### Exercice 5  Les exceptions

Vous avez peut-être remarqué que les Exception déclenchées par `throw` ne
fonctionnent plus. En effet, au sein de Lumen, il faut déclarer que les
exception sont des exceptions de base de PHP et non pas des Exceptions de
Lumen.

Cela se fait très simplement : ajoutez un `\` devant chaque instance d'un
objet `Exception` dans `UserController.php` et dans le modèle `User.php`.
