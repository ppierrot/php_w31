## Liste de courses

Dans les TP précédent, vous avez mis en place la logique MVC d'une partie de l'application, permettant de gérer les utilisateurs.
- gestion de comptes
  - Créer un compte utilisateur
  - Se connecter
  - Se déconnecter
  - Modifier mot de passe
  - supprimer le compte utilisateur

A l'aide du framework Lumen, réaliser une application qui proposera les fonctionnalités suivantes pour un utilisateur connecté :
  - gestion des listes de courses
    - Voir l'ensemble de ses listes de courses
    - Créer une nouvelle liste de courses. Une liste est définie par un titre.
    - Supprimer une liste de courses
    - modifier le nom d'une liste de courses
  - pour chaque liste de courses
    - Ajouter un nouvel élément à une liste. Un élément a un nom et un état. Chaque élément peut être soit dans l'état "A acheter", soit dans l'état "Dans le caddy".
    - Voir la liste des éléments d'une liste
    - Modifier l'état d'un élément (passer de "A acheter" à "Dans le caddy", et inversement)

Il sera nécessaire de définir les routes , les middleware, les modèles, les contrôleurs et les vues (définies pour le moteur de vue Blade) nécessaire au bon fonctionnement de l'application.

Un exemple de l'application attendue est disponible à l'adresse suivante : http://marvin.u-strasbg.fr


to do :


(à implémenter) fonctions dans classes
(à implémenter) controleurs 
(a implémenter)faire les vues de création/suppréssion et modif
(a implémenter)faire les routes
middleware pour vérifier si utilisateur dans ses listes à lui
