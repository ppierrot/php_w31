TP7 - Le moteur de vues Blade
=============================

Dans le TP précédent, nous avons mis en place la logique MVC de notre
application.

Dans ce TP7, l'objectif est de factoriser le code de nos vues grâce au
moteur de vues développé par Laravel : Blade.

Note : La documentation sur Blade est disponible uniquement dans la
       documentation de Laravel ici :
       [https://laravel.com/docs/5.5/blade](https://laravel.com/docs/5.5/blade)

### Exercice

Pour ce TP7, vous devez simplement respecter les contraintes suivantes :

- avoir un layout principal étendu par **toutes vos vues**
- intégrer dans le layout principal :
    - un titre personnalisable dans les vues
    - un contenu central personnalisable dans les vues
    - le code relatif aux messages d'alertes
- le layout principal doit également contenir un menu en entête
  du corps de la page :
    - des liens vers `signin` et `signnp` si la variable PHP `$connected`
      n'existe pas
    - un lien vers `signout` si la variable PHP `$connected` existe.

Adaptez le routage, les contrôleurs et les middleware en conséquence.
