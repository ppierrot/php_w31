TP5 - Prise en main de Lumen
============================

Préambule
---------

Intégrer votre application d'authentification au framework Lumen.
Vous pouvez vous baser au choix :

- sur de vos propres sources résultant du TP4
- sur l'archive [PropositionTP4.tar.gz](PropositionTP4.tar.gz)


Exercice 1 : Installation
-------------------------

Suivez les indications donnés dans
[Lumen/installation.md](Lumen/installation.md).


Exercice 2 : Préparation des vues
---------------------------------

1. Copiez les fichiers suivants du TP4 dans `resources/views/` :
    - `adduser.php`
    - `authenticate.php`
    - `changepassword.php`
    - `deleteuser.php`
    - `formpassword.php`
    - `signin.php`
    - `signout.php`
    - `signup.php`
    - `welcome.php`

2. Dans `routes/web.php` :
    - Écrivez les routes GET et POST pour les fichiers ci-dessus
    - Faites en sorte qu'une requête vers la racine du site propose la vue
      `signin.php`
    - Testez toutes les routes GET. Regardez ce qu'il se passe lorsqu'on
      demande une route qui n'a pas été prévue dans `routes/web.php`

3. Corriger les liens (balise `<a>`) ainsi que les directives `header(...)`
   dans les fichiers `signin.php`, `signup.php`, `formpassword.php` et
   `welcome.php`

4. - Lisez le paragraphe sur les redirections dans la documentation Lumen ici :
   https://lumen.laravel.com/docs/5.5/responses#redirects
   - Dans chacun des fichiers ci-dessus, remplacer toutes les conditions de
     test sur les `REQUEST_METHOD` par des routes avec redirections dans le
     fichier `routes/web.php`.


Exercice 3 : Modèles et BDD
----------------------------

1. Modifiez le fichier `.env` en précisiant les champs `DB_USERNAME` et
   `DB_PASSWORD`. Ajouter le champ `DB_DSN` correspondant au champ `SQL_DSN` de
   l'ancien fichier `bdd.php`

2. Copier le répertoire `models` du TP4 dans le répertoire `app` de votre
   application Lumen. Il doit maintenant se nommer `app/Models`.

3. Modifier les deux fichiers du répertoire `app/Models` :
    - renommez-les avec une majuscule. Le nom du fichier doit être identique à
      celui de la classe, avec la même casse.
    - ajoutez le bon namespace (voir par exemple le fichier
      `app/Http/Controllers/Controller.php`)
    - indiquer l'utilisation du namespace PDO (instruction `use`)

4. Dans chacun des fichiers `adduser.php`, `authenticate.php`,
   `changepassword.php`, `deleteuser.php` et `signout.php` :
    - corrigez les directives `header(...)`
    - supprimez l'inclusion des fichiers `bdd.php`
    - modifiez la création de l'objet PDO en y incluant un appel aux variables
      d'environnement du fichier `.env`. La doc est ici :
      https://lumen.laravel.com/docs/5.5/configuration
    - remplacez l'appel au fichier `models/user.php` par les directives `use`
      pour les classe `Model_Base` et `User`


Exercice 4 : Aller plus loin avec les routes
--------------------------------------------

1. Lisez la documentation sur les routes ici :
https://lumen.laravel.com/docs/5.5/routing

2. Faites en sorte de créer un groupe de requêtes de préfixe `account` pour les
   pages suivantes :
    - `changepassword.php`
    - `deleteuser.php`
    - `formpassword.php`
    - `signout.php`
    - `welcome.php`

Pensez à mettre à jour les directives `header` dans les fichiers concernés si
besoin.
