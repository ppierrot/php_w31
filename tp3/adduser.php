<?php

  require('bdd.php');

  // si requête passé en post, si id, mdp et conf_mdp existent
  if($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['id']) && isset($_POST['mdp']) && isset($_POST['conf_mdp'])){
    
    session_start();

    //si mot de passe = confirmation de mot de passe
    if(strcmp($_POST['mdp'], $_POST['conf_mdp']) !== 0){
      header('Location: signup.php');
      exit();
    }

    //protège l'intégrité des balises html enregistrées
    $login = htmlentities($_POST['id']);
    $password = htmlentities($_POST['mdp']);
    $conf_pass = htmlentities($_POST['conf_mdp']);

    $result = $db->prepare("INSERT INTO Users (login, password) values (:login, :password)");

    $ok1 = $result->bindValue( ':login', $login, PDO::PARAM_STR); 
    $ok2 = $result->bindValue( ':password', password_hash($password, PASSWORD_DEFAULT), PDO::PARAM_STR);
    $ok3 = $result->execute();

    if($ok3 == true){
      header('Location: signin.php');
      exit();
    }
    else{
      header('Location: signup.php');
      exit();
    }

  }

?>