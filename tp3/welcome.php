<?php
  session_start();

  if(!isset($_SESSION['login'])){
    header('Location: signin.php');
    exit();
  }

?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Mon compte</title>
    </head>
    <body class="container">
      <h1 class="center-align">Bienvenue, <?= $_SESSION['login']; ?> !</h1>
      <a href="signout.php">Deconnexion</a>
    </body>
</html>