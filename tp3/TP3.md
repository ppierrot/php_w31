TP3 - Gestion d'utilisateurs et BDD
===================================

Créez dans votre clone Git un répertoire `TP3` et copiez-y les 4 fichiers
suivant du `TP2` :
- `signin.php`
- `authenticate.php`
- `welcome.php`
- `signout.php`

Cet exercice vous propose de mettre en place la gestion des utilisateurs dans
une base de données.

*Note* : il est fortement recommandé d'avoir réalisé l'exercice 5 du TP2 afin
de transmettre les message d'erreur de php au client.


Exercice 1 : Mise en place de la BDD
------------------------------------

1. Créer une nouvelle base de données `W31` à partir de
[SS4S](http://ss4s.iutrs.unistra.fr)
2. Créer une table `Users` permettant de stocker les logins et mot de passe des
utilisateurs. Réfléchissez bien aux champs et attributs des champs :
auto-increment, unique, etc.
3. Ajouter manuellement un ou plusieurs utilisateurs


Exercice 2 : Authentification
-----------------------------

1. Créez un nouveau fichier `bdd.php` contenant 3 constantes correspondant aux
3 paramètres de la construction d'un objet PDO (voir cours)
2. Modifiez le fichier `authenticate.php` pour qu'il authentifie les
utilisateurs à partir de la BDD en utilisant PDO. Ceci remplace l'utilisation
du fichier `users.php` du TP précédent.

*Note* : Pensez à gérer les exceptions PHP déclenchées par la construction
d'un objet PDO. Si vous avez fait l'exercice 5 du TP2, vous pouvez ajouter ces
messages d'erreur à la variable de session `message`.


Exercice 3 : Inscription
------------------------

1. Écrivez un nouveau fichier `signup.php` qui propose un formulaire
d'inscription pour un nouvel utilisateur et le soumet à la page `adduser.php`.
2. Écrivez un nouveau fichier `adduser.php` qui effectue les traitements
suivants :
    - il vérifie que la méthode utilisée pour l'appeller est `POST`
    - il vérifie et sécurise les champs du formulaire de `signup.php`
    - il demande une redirection vers `signup.php` si le mot de passe et sa
      confirmation diffèrent
    - il tente d'insérer le nouvel utilisateur :
        - si la requête s'est bien passée, il demande une redirection vers
        `signin.php`
        - sinon il demande une redirection vers `signup.php`
3. Pour plus de navigabilité, ajoutez un lien pour s'inscrire sur `signin.php`
et, inversement, un lien pour se connecter sur `signup.php`.

*Note 1* : Si vous avez fait l'exercice 5 du TP2, vous pouvez ajouter les
messages d'erreur et de réussite à la variable de session `message`.


Exercice 4 : Mots de passes cryptés
-----------------------------------

Actuellement les mots de passe sont codés en clair dans votre base de données.
Vous allez donc mettre en place le cryptage (et le décryptage) des mots de
passe.

*Note* : Si tout a bien été fait jusque là, les questions 2. et 3. de cet
exercice nécessite de ne changer **qu'une seule ligne** dans chacun des
fichiers.

1. Supprimez de votre BDD tous les utilisateurs inscrits, via PhpMyAdmin.
2. Modifiez le fichier `adduser.php` afin qu'il enregistre le mot de passe
chiffré avec la fonction PHP
[`password_hash`](http://php.net/manual/fr/function.password-hash.php).
Attention, lisez bien sa documentation et, si besoin, procédez aux
modifications des attributs de la colonne du mot de passe dans PhpMyAdmin.
3. Modifiez le fichier `authenticate.php` pour qu'il compare le mot de passe
du formulaire avec celui récupéré dans la BDD à l'aide de la fonction
[`password_verify`](http://php.net/manual/fr/function.password-verify.php).


Exercice 5 (bonus) : Vérification des mots de passe côté client
---------------------------------------------------------------

En théorie, la vérification de la similarité des deux mots de passe
s'effectue plutôt côté client, en Javascript.

1. Dans `signup.php`, écrivez une fonction javascript `checkPassword` qui
récupère le contenu des `input` du mot de passe et de sa confirmation et les
compare. Utilisez la fonction `setCustomValidity` pour mettre à jour la
validité du champ de confirmation.
2. Faite exécuter la fonction `checkPassword` à chaque entrée d'un nouveau
caractère dans le champ de confirmation. Regardez du côté de l'attribut HTML
`oninput`.


Pour les plus rapides
---------------------

- Niveau 1 : Ajoutez un fichier de style CSS pour mettre en forme votre
formulaire.
- Niveau 2 : Utilisez [Materialize](https://materializecss.com/) pour mettre en
forme vos différentes pages.
