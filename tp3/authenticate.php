<?php
  session_start();

  if(isset($_GET['login']) && isset($_GET['password'])){

    include 'bdd.php'; //on peut aussi utiliser 'require', la diff c'est qu'il balancera une érreur si ya pas. En tout cas on fait appel à bdd.php pour se connecter à la base de données

    //protège l'intégrité des balises html enregistrées
    $login = htmlentities($_GET['login']);
    $password = htmlentities($_GET['password']);

    //on prépare une requête sql 
    $result = $db->prepare("SELECT login, password FROM Users WHERE login = :login");

    //on lie les variables d'intégrité html à ceux utilisés dans la requête sql puis on éxécute la requête 
    $ok1 = $result->bindValue( ':login', $login, PDO::PARAM_STR);
    $ok2 = $result->execute();

    //si la requête a bien été exécuté, ça veut dire que login et mot de passe sont correct et on renvoi vers welcome.php
    if ($ok2 == true){

      $row = $result->fetch(PDO::FETCH_ASSOC); // transforme résultat inexploitable de la requête en tableau 

      //vérification mot de passe déhaché = mot de passe entré dans signin
      if(password_verify($password, $row['password'])){
        $_SESSION['login'] = $login;
        header('Location: welcome.php');
        exit();
      }
      else{
        header('Location : signin.php');
        exit();
      }
    } 
    else {
      header('Location: signin.php');
      exit();
    }
  }
?>