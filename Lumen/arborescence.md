Comment ça marche
-----------------

### Composer

C'est un outils de gestion des dépendances PHP :
1. On déclare les bibliothèques dont dépend notre projet dans le fichier de
   configuration `composer.json` à la racine de notre projet
2. À l'installation, les bibliothèques sont installées dans le répertoire
  `vendor`
3. Pour utiliser une nouvelle bibliothèque, on la déclare dans `composer.json`
   puis on lance la commande suivante à la racine du site :
   ```
   $ composer update
   ```

### Le serveur de dev php

Il permet de tester son application rapidement en local.
Si vous travaillez sur `webetu`, vous n'en avez pas besoin.

Il se lance avec la commande suivante :
```
$ php -S localhost:1234 -t test/public/
```

En savoir plus : http://php.net/manual/fr/features.commandline.webserver.php


Les fichiers et répertoires utiles
----------------------------------

Pour accéder à votre site, il faut faire une requête vers le fichier
`index.php` du répertoire `public`. Peuvent alors être appelés les fichiers
suivants :

- .env
    - fichier de configuration de l'application
    - contient les variables pour l'accès à la BDD

- public/index.php
    - fichier d'entrée de l'application
    - récupère l'instance de l'application lumen définie dans
      `bootstrap/app.php` et l'exécute (méthode `run()`)

- bootstrap/app.php
    - crée une nouvelle instance de l'application lumen
    - initialise les routes de l'application qui ont été définies dans
      `routes/web.php`
    - prend en compte les contrôleurs définis dans le répertoire
      `app/Http/Controllers`
    - on peut le modifier pour y ajouter un Middleware

- routes/web.php
    - défini l'ensemble des routes pouvant être atteintes par les utilisateurs
    - fait appel aux middleware et contrôleurs lorsque cela est nécessaire

Middleware
- doivent être placé dans `app/Http/Middleware`

Controllers
- doivent être placé dans `app/Http/Controller`
