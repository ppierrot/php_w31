Lumen
=====

Installation sur webetu
------------------------

1. Initialisation d'une application `[NAME]` basée sur lumen
  ```
  $ cd ~/public_html/
  $ composer create-project --prefer-dist laravel/lumen [NAME] "5.5.*"
  ```

2. Votre site est accessible à l'adresse
   `https://webetu.iutrs.unistra.fr/~[VOTRE LOGIN]/[NAME]/public/`

Si tout s'est bien passé, vous devez voir à l'adresse ci-dessus une page blanche
ne comportant que le texte suivant :
```
 Lumen (5.5.2) (Laravel Components 5.5.*)
```


Installation sur votre ordi personnel
--------------------------------------

### Dépendances

- php-fpm
- php-zip
- php-xml
- php-mbstring

### Procédure

1. Installation manuelle de Composer
  ```
   $ sudo apt install composer
  ```

2. Initialisation d'une application `[NAME]` basée sur lumen
  ```
   $ composer create-project --prefer-dist laravel/lumen [NAME] "5.5.*"
  ```

3. Exposition du site sur le serveur web
  ```
  $ php -S localhost:1234 -t [NAME]/public/
  ```

Si tout s'est bien passé, vous devez voir, à l'adresse `http://localhost:1234`,
une page blanche ne comportant que le texte suivant :
```
 Lumen (5.5.2) (Laravel Components 5.5.*)
```
