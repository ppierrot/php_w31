<?php
	session_start();
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Signup</title>
	</head>
	<body>
		<h1>Signup</h1>
		<p>If you still have an account, please <a href="signin">signin</a>.</p>
		<form action="adduser" method="post">
			<label for="login">Login</label>              <input type="text"     id="login"    name="login"    required autofocus>
			<label for="password">Password</label>        <input type="password" id="password" name="password" required>
			<label for="confirm">Confirm password</label> <input type="password" id="confirm"  name="confirm"  required>
			<input type="submit" value="Connect me">
		</form>
<?php
	if ( isset($_SESSION['message']) && !empty($_SESSION['message']))
	{
		echo "<div class=\"message\">" . $_SESSION['message'] . "</div>";
		unset($_SESSION['message']);
	}
?>
	</body>
</html>
