<?php
	// Si la requête arrive avec un autre type que GET
	// ou que le client n'est pas considéré comme connecté
	if ( !isset($_SESSION['user']) )
	{
		// on envoie une demande de redirection en GET vers signin.php
		header('Location: /signin');
		exit;
	}

	// sinon, on affiche la page de bienvenue
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>My account</title>
    </head>
    <body>
        <p>
			Hello <?= $_SESSION['user']; ?> !<br>
			Welcome on your account.
		</p>
		<p><a href="formpassword">Change my password</a></p>
		<p><a href="deleteuser">Delete my account</a></p>
		<p><a href="signout">Sign out</a></p>

<?php
	if ( isset($_SESSION['message']) && !empty($_SESSION['message']))
	{
		echo "<div class=\"message\">" . $_SESSION['message'] . "</div>";
		unset($_SESSION['message']);
	}
?>
    </body>
</html>
