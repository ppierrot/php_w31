<?php
if ( !isset($_SESSION['user']) )
{
    // on envoie une demande de redirection en GET vers signin.php
    header('Location: /signin');
    exit;
}
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Change password</title>
	</head>
	<body>
		<h1>Change password</h1>
		<p>Go back to <a href="welcome">home</a>.</p>
		<form action="changepassword" method="post">
			<label for="newpassword">New password</label>         <input type="password" id="newpassword"     name="newpassword"      required>
			<label for="confirmpassword">Confirm password</label> <input type="password" id="confirmpassword" name="confirmpassword"  required>
			<input type="submit" value="Change my password">
		</form>
<?php
	if ( isset($_SESSION['message']) && !empty($_SESSION['message']))
	{
		echo "<div class=\"message\">" . $_SESSION['message'] . "</div>";
		unset($_SESSION['message']);
	}
?>
	</body>
</html>
