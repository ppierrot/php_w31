<?php
	session_start();
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Signin</title>
	</head>
	<body>
		<h1>Signin</h1>
		<p>If you don't still have an account, please <a href="signup">signup</a>.</p>
		<form action="authenticate" method="post">
			<label for="login">Login</label>       <input type="text"     id="login"    name="login"    required autofocus>
			<label for="password">Password</label> <input type="password" id="password" name="password" required>
			<input type="submit" value="Connect me">
		</form>
<?php
	if ( isset($_SESSION['message']) && !empty($_SESSION['message']))
	{
		echo "<div class=\"message\">" . $_SESSION['message'] . "</div>";
		unset($_SESSION['message']);
	}
?>
	</body>
</html>
