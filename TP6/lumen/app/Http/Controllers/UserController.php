<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Model_Base;
use App\Models\User;

use PDO;

class UserController extends Controller
{
    // Controleur vide
    public function __construct() {}

    public function authenticate( Request $request )
    {
        if ( !$request->has('login') || !$request->has('password') )
        {
            return redirect('signin');
        }

        $login = htmlentities($request->input('login'));
        $password = htmlentities($request->input('password'));

        //-----------------------------------------------------------------------------
        $user = new User( $login, $password );
        //-----------------------------------------------------------------------------

        if ( !$user->exists() )
        {
            $_SESSION['message'] = "Wrong login or password .";
            return redirect('signin');
        }

        $_SESSION['user'] = $login;
        return redirect('account/welcome');
    }

    public function adduser( Request $request )
    {
        if ( !$request->has('login') || !$request->has('password') || !$request->has('confirm') )
        {
            return redirect('signup');
        }

        $login = htmlentities($request->input('login'));
        $password = htmlentities($request->input('password'));
        $confirm = htmlentities($request->input('confirm'));

        if ( $password != $confirm )
        {
            $_SESSION['message'] = "Password and confirmed password are different.";
            return redirect('signup');
        }

        //---------------------------------------------------------------------
        $user = new User( $login, $password );
        //---------------------------------------------------------------------

        try {
            $user->create();
            // Si la requête a été exécutée avec succès
            $_SESSION['message']  = 'Congratulations ' . $login . ', Account successfully created !<br>';
            $_SESSION['message'] .= ' Please signin to access to your account.';
        }
        catch (\Exception $e)
        {
            // Si la requête a échoué, c'est que le login existe déjà
            $_SESSION['message'] = $e;
        }

        return redirect('signin');
    }

    public function signout()
    {
        // on détruit la variable de session 'user'
        unset($_SESSION['user']);
        // et on envoie une demande de redirection en GET vers signin.php
        return redirect('/signin');
    }

    public function changePassword( Request $request )
    {
        if ( !isset($_SESSION['user']) || !$request->has('newpassword') ||
             !$request->has('confirmpassword') )
        {
            return redirect('account/formpassword');
        }

        $login           = $_SESSION['user'];
        $newpassword     = htmlentities($request->input('newpassword'));
        $confirmpassword = htmlentities($request->input('confirmpassword'));

        if ( $newpassword != $confirmpassword )
        {
            $_SESSION['message'] = "Error: the two passwords are different.";
            return redirect('account/formpassword');
        }

        //---------------------------------------------------------------------
        $user = new User( $login );
        //---------------------------------------------------------------------

        try {
            $user->changePassword( $newpassword );
        }
        catch (\Exception $e)
        {
            $_SESSION['message'] = $e;
            return redirect('account/formpassword');
        }

        // Si la requête a été exécutée avec succès
        $_SESSION['message'] = "Password successfully updated.";
        return redirect('account/welcome');
    }

    public function deleteUser( Request $request )
    {
        if ( !isset($_SESSION['user']) )
        {
            return redirect('account/welcome');
        }

        $login = $_SESSION['user'];

        //---------------------------------------------------------------------
        $user = new User( $login );
        //---------------------------------------------------------------------

        try {
            $user->delete();
        }
        catch (\Exception $e)
        {
            $_SESSION['message'] = $e;
            return redirect('account/welcome');
        }

        // Si la requête a été exécutée avec succès
        session_destroy();
        session_start();
        $_SESSION['message'] = "Account successfully deleted.";
        return redirect('signin');
    }
}
