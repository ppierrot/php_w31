<?php

namespace App\Models;
use PDO;

class User extends Model_Base
{
	private $_login;
	private $_password;

	const USER_TABLE = "Users";

	public function __construct( $login, $password = '' )
	{
		$this->set_login($login);
		$this->set_password($password);
	}

	public function login() : string
	{
		return $this->_login;
	}

	public function set_login( string $login )
	{
		$this->_login = $login;
	}

	public function password() : string
	{
		return $this->_password;
	}

	public function set_password( string $password )
	{
		$this->_password = $password;
	}

	public function exists() : bool
	{
		$q = self::$_db->prepare('SELECT password FROM '.User::USER_TABLE.' WHERE login = :login');
	    $ok =  $q->bindValue(':login', $this->_login, PDO::PARAM_STR);
	    $ok &= $q->execute();

		if ($ok)
		{
			$user = $q->fetch(PDO::FETCH_ASSOC);
			$ok = $user && password_verify($this->_password,$user['password']);
		}

		return $ok;
	}

	public function create()
	{
		$q = self::$_db->prepare('INSERT INTO '.User::USER_TABLE.' SET login = :login, password=:password');
	    $ok =  $q->bindValue(':login',    $this->_login,    PDO::PARAM_STR);
	    $ok &= $q->bindValue(':password', password_hash($this->_password,PASSWORD_DEFAULT), PDO::PARAM_STR);
	    $ok &= $q->execute();

		if ( !$ok )
			throw new \Exception("Error: user creation in DB failed.");
	}

	public function changePassword( string $newpassword )
	{
		$q = self::$_db->prepare('UPDATE '.User::USER_TABLE.' SET password = :password WHERE login = :login');
		$ok =  $q->bindValue(':login',    $this->_login, PDO::PARAM_STR);
		$ok &= $q->bindValue(':password', password_hash($newpassword,PASSWORD_DEFAULT), PDO::PARAM_STR);
		$ok &= $q->execute();

		if ( !$ok )
			throw new \Exception("Error: user updating in DB failed.");
		else
			$this->_password = $newpassword;
	}

	public function delete()
	{
		$q = self::$_db->prepare('DELETE FROM '.User::USER_TABLE.' WHERE login = :login');
		$ok =  $q->bindValue(':login', $this->_login, PDO::PARAM_STR);
		$ok &= $q->execute();

		if ( !$ok )
			throw new \Exception("Error: user deletion from DB failed.");
	}
}
