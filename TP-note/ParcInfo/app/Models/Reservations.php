<?php

  namespace App\Models;

  use PDO;

  class Reservations extends Model_base{

    /*membres*/

    private $_user_id;
    private $_poste_id;
    private $_date_debut;
    private $_date_fin;
    private $_est_root;
    const RESERV_TABLE = "Reservations";

    /*constructeur de la classe*/

    function __construct(string $user_id, int $poste_id, date $date_debut, date $date_fin, bool $est_root) { 
      
      $this->_user_id = $user_id; 
      $this->_poste_id = $poste_id; 
      $this->_date_debut = $date_debut;
      $this->_date_fin = $date_fin;
      $this->_est_root = ;
    }

    /*getters*/

    function getName(){ 
      return $this->_name; 
    }

    function getState(){
      return $this->_state;
    }

    function getlistId(){
      return $this->_listId;
    }

    /*setters*/

    function setName(string $name){ 
      $this->_name = $name; 
    }

    function setState(bool $state){
      $this->_state = $state;
    }

    function setProprio(int $listId){
      $this->_listId = $listId;
    }

    /*méthodes*/
  
    /*ajouter un article à la liste*/
    public function newReservation() {
      
      
    }

    /*affiche le nom des articles de la liste*/
    public function afficher(){

    }

  }

?>