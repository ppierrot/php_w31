<?php

  namespace App\Models;

  use PDO;
  use App\Models\Reservations;

  class Poste extends Model_base{

    /*membres*/

    private $_salle;
    private $_OS;
    const POSTE_TABLE = "poste";

    /*constructeur de la classe*/

    function __construct(int $salle, string $OS) { 
      
      $this->_salle = $salle; 
      $this->_OS = $OS;
    
    }

    /*getters*/

    function getSalle(){ 
      return $this->_salle; 
    }

    function getOS(){ 
      return $this->_OS; 
    }

    /*setters*/

    function setSalle(int $salle){ 
      $this->_salle = $salle; 
    }

    function setOS(string $OS){ 
      $this->_OS = $OS; 
    }

    /*méthodes*/
  
    /*Créé un nouveeau poste dans la bdd*/
    public function create() {

      $result = model_base::$_db->prepare("INSERT INTO " . course::POSTE_TABLE . " (salle, os) values (:salle, :os)");

      $ok1 = $result->bindValue( ':salle', $this->_salle, PDO::PARAM_INT); 
      $ok2 = $result->bindValue( ':os', $this->_OS, PDO::PARAM_STR); 

      try{
        $ok3 = $result->execute();
      } catch(Exception $e){
        echo $e->getMessage();
      }

      return redirect("/account/listepostes");

    }

    /*récupérer informations pour affichage*/
    public function recup(){
      //test
      //echo $this->_salle;

      $result = model_base::$_db->prepare("SELECT id FROM " . course::POSTE_TABLE . " WHERE id = :postId");
      
      $ok1 = $result->bindValue( ':postId', $this->_postId, PDO::PARAM_INT); 
      
      try{
        $ok2 = $result->execute();
      } catch(Exception $e){
        echo $e->getMessage();
      }

      return view('postes');
    }

    /*supprimer un poste*/
    public function delete(){


    }

  }

?>