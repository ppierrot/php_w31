<?php

  namespace App\Http\Controllers;

  use App\Models\Poste;
  use App\Models\Reservations;
  use App\Models\Model_base;
  use PDO;
  use Illuminate\Http\Request;


  class CourseController extends Controller
  {
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function addPoste(Request $request)
    {

      //si pas login utilisateur ou pas de nom en post
      if(!$request->has('room_poste') ||!$request->has('os'){
        echo'manque information';
      }

      //protège l'intégrité des balises html enregistrées
      $room_poste = htmlentities($request->input('room_poste'));
      $os = htmlentities($request->input('os'));

      //création objet
      $obj = new Poste($room_poste, $os);

      $obj->create();
    
      redirect('/account/listepostes');
    }

    public function recuperer(Request $request){

      if(!isset($_SESSION['login'])){
        echo'nom utilisateur non renseigné'; 
      }

      //protège l'intégrité des balises html enregistrées
      $login = htmlentities($_SESSION['login']);

      $obj = new Course($login, "");

      $obj->recup();

      return view('listes_courses');
    }


    //récupérer nom d'utilisateur
    //modifList
    //addArticle
    //articleStat
  }

?>