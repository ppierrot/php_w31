<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\Model_Base;
use PDO;

class SessionMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // Démarre la session pour toutes les requêtes
        session_start();
        // Supression des anciens messsages
        unset($_SESSION['message']);

        // Prépare l'objet PDO pour les classes model
        try
        {
            $db = new PDO(env('DB_DSN'), env('DB_USERNAME'), env('DB_PASSWORD'));
        }
        catch (PDOException $e)
        {
            $_SESSION['message'] = $e->getMessage();
            return redirect('signin');
        }

        // Affectation de l'objet PDO à l'instance statique de Model_Base
        Model_Base::set_db( $db );

        // Suite de la requête
        return $next($request);
    }
}
