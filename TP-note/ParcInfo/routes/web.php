<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

//groupe de routes avec un préfixe "account" d'ajouté et passant par le middleware session
$router->group( ['prefix' => 'account', 'middleware' => 'session'], function () use ($router) {

    $router->get('/listepostes', [
        'uses' => 'PosteController@recuperer'
    ]);

    $router->post('/addPoste', [
        'uses' => 'PosteController@addPoste'
    ]);

    $router->get('/addPoste', function () {
        return view('formPoste');
    });

    // /listepostes get vers Postecontroler@deleteList
    // /listepostes get vers Postecontroler@modifList
    // /[name_poste]/reservations get vers Postecontroler@addReservation
    // /[name_poste]/reservations get vers Postcontroler@deleteReservation

    $router->get('/deleteuser', [
        'uses' => 'UserController@deleteUser'
    ]);

    $router->post('/deleteuser', function () {
        redirect('welcome');
    });

    $router->get('/formpassword', function () {
        return view('formpassword');
    });

    $router->post('/formpassword', function () {
        redirect('signin');
    });

    $router->post('/changepassword', [
        'uses' => 'UserController@changepassword'
    ]);

    $router->get('/changepassword', function () {
        redirect('formpassword');
    });

    $router->get('/signout',[
        'uses' => 'UserController@signout'
    ]);

    $router->get('/welcome', function () {
        return view('welcome');
    });

    $router->post('/welcome', function () {
        redirect('signin');
    });
});

$router->get('/', function () use ($router) {
    return view('signin');
});

$router->post('/', function () {
    redirect('signin');
});

$router->post('/adduser', [
    'middleware' => 'session', 'uses' => 'UserController@adduser'
]);

$router->get('/adduser', function () {
    redirect('signin');
});

//si route emprunté, redirige vers fonction authenticate dans usecontroller, plus un middleware "session" non global, attribué manuellement aux routes
$router->post('/authenticate', [
    'middleware' => 'session', 'uses' => 'UserController@authenticate'
]);

$router->get('/signin', function () {
    return view('signin');
});

$router->get('/signup', function () {
    return view('signup');
});