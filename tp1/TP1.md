TP1 : requêtes GET/POST, cookies et sessions
============================================

Exercice 1 - Bonjour
--------------------

1. Écrire un fichier PHP `formulaire.php` qui renvoie un document HTML avec un
formulaire demandant à l'utilisateur son nom et son prénom. Il doit avoir pour
action `bonjour.php`.
2. Écrire un script PHP `bonjour.php` capable de réceptionner ce formulaire
soumis par une requête HTTP POST, et d'intégrer un paragraphe souhaitant la
bienvenue à cet utilisateur au sein du document HTML renvoyé.
3. Écrire un fichier script `formulaireBonjour.php` qui fusionne les deux
précédents :
    - l'attribut `action` de son formulaire pointe sur lui-même
    - il produit du code HTML contenant le formulaire OU le message de
    bienvenue selon la présence ou non des variables dans le tableau `$_POST`.

Exercice 2 - Générer des boutons
--------------------------------

1. Écrire un fichier PHP `boutons.php` qui renvoie un document HTML contenant
10 boutons.
2. Écrire un fichier PHP `boutonsGet.php` similaire à `boutons.php`, où le
nombre de boutons à générer est transmis via l'URL par le client (c'est la
méthode GET du protocole HTTP).
3. Écrire un fichier PHP `boutonsPost.php` similaire à `boutons.php`, où le
nombre de boutons à générer est transmis via un formulaire par le client
(c'est la méthode POST du protocole HTTP).

Exercice 3 - Compteur de visites
--------------------------------

1. Écrire un fichier PHP `compteur.php` qui :
    - crée ou rétablit une session PHP pour le client
    - initialise ou incrémente la variable qui sert de compteur
    - renvoie un document HTML indiquant le nombre de requêtes vers ce
    fichier depuis le début de la session de l'utilisateur.

    À chaque nouvelle requête de la page `compteur.php`, le chiffre indiqué
    doit augmenter de 1.

2. Ajouter, dans le document HTML renvoyé, un lien vers un fichier PHP
`resetCompteur.php`. Ce script PHP effectue les deux actions suivantes :
    - il réinitialise le compteur définit dans le fichier `compteur.php`
    - puis il demande au client de se rediriger vers le fichier `compteur.php`.

Note : Pour la redirection vers le fichier `compteur.php`, regarder dans le doc
de [php.net](http://php.net) la directive
[`header`](http://php.net/manual/fr/function.header.php).
