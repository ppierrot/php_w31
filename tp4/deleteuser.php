<?php
  
  require('models/user.php');
  require('bdd.php');
  model_base::set_db($db);

  session_start();

  //si utilisateur connécté et si requêtes de type get
  if(isset($_SESSION['login']) && $_SERVER['REQUEST_METHOD'] === 'GET'){

    //création objet user
    $obj = new user($_SESSION['login'], "");

    $obj->delete();

    // on détruit la session en cours
    session_destroy();
    header('Location: signin.php');
    exit();
    
  }
  else{
    header('Location: welcome.php');
    exit();
  }

?>