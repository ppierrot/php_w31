<?php

  require('models/user.php');
  require('bdd.php');

  session_start();

  model_base::set_db($db); //on enregistre le PDO de notre base de données dans une fonction de model_base qui perméttra de rester connéctée sur chaque classes héritant de model_base (plus besoin de faire systématiquement appel à bdd.php)

  //si requêtes post du login et password existent
  if(isset($_POST['login']) && isset($_POST['password'])){

    //protège l'intégrité des balises html enregistrées
    $login = htmlentities($_POST['login']);
    $password = htmlentities($_POST['password']);

    //création objet user
    $obj = new user($login, $password);

    //si le résultat retourné par l'appel de la fonction exists est true
    if($obj->exists() == true){
      $_SESSION['login'] = $login;
      header('Location: welcome.php');
      exit();
    }
    else{
      header('Location: signin.php');
      exit();
    }
  }
  else{
    header('Location: signin.php');
    exit();
  }
?>