<?php
  
  require('models/user.php');
  require('bdd.php');
  model_base::set_db($db);
  
  session_start();

  // si requête passé en post, si id, mdp et conf_mdp existent
  if($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['id']) && isset($_POST['mdp']) && isset($_POST['conf_mdp'])){

    //si mot de passe = confirmation de mot de passe
    if(strcmp($_POST['mdp'], $_POST['conf_mdp']) !== 0){
      header('Location: signup.php');
      exit();
    }

    //protège l'intégrité des balises html enregistrées
    $login = htmlentities($_POST['id']);
    $password = htmlentities($_POST['mdp']);

    //création objet user
    $obj = new user($login, $password);

    $obj->create();
    
    header('Location: signin.php');

    exit();

  }

?>