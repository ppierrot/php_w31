<?php
       
  require('models/user.php');
  require('bdd.php');
  model_base::set_db($db);

  session_start();

  //si utilisateur est connécté et si requêtes de type post
  if(isset($_SESSION['login']) && $_SERVER['REQUEST_METHOD'] === 'POST'){

    //si variables dans requête POST existent
    if(!isset($_POST['new_mdp']) || !isset($_POST['conf_new_mdp'])){
      header('Location: formpassword.php');
      exit();
    }

    //si mot passe = confirmation mot de passe
    if($_POST['new_mdp'] === $_POST['conf_new_mdp']){

      //protège l'intégrité des balises html enregistrées
      $login = htmlentities($_SESSION['login']);
      $new_mdp = htmlentities($_POST['new_mdp']);

      //création objet user
      $obj = new user($login, $new_mdp);

      $obj->changePassword($new_mdp);

      header('Location: welcome.php');
        
      exit();
    }
    else{
      header('Location: formpassword.php');
      exit();
      
    }
  } 
  else {
    header('Location: formpassword.php');
    exit();
  }

?>