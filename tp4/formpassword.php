<?php
  session_start();
  if(isset($_SESSION['login']) && $_SERVER['REQUEST_METHOD'] === 'GET'){
 ?>

<!DOCTYPE html>
  <html>
    <form action="changepassword.php" method="post">
      <div>
        <label for="mdp">Nouveau mot de Passe :</label>
        <input type="password" name="new_mdp">
        <label for="mdp">Confirmer mot de passe :</label>
        <input type="password" name="conf_new_mdp">
        <input type="submit" value="modifier">
      </div>
      <div>
        <a href="welcome.php">S'identifier</a>
      </div>
    </form>
  </html>

<?php 
  } 
  else {
    header('Location: signin.php');
    exit();
  }  
?>