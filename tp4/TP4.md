TP4 - Gestion d'utilisateurs et POO
===================================

Créez dans votre clone Git un répertoire `TP4` et copiez-y les 7 fichiers
suivant du `TP3` :
- `authenticate.php`
- `adduser.php`
- `bdd.php`
- `signin.php`
- `signout.php`
- `signup.php`
- `welcome.php`


Exercice 1 : Modification du mot de passe
-----------------------------------------

Cet exercice propose d'ajouter la fonctionnalité de changement de mot de passe
avec deux nouveaux fichiers : la vue `formpassword.php` et le contrôleur
`changepassword.php`.

1. Créez un nouveau fichier `formpassword.php` contenant un formulaire avec
deux champs : son son nouveau mot de passe et la confirmation du nouveau mot de
passe. Ce formulaire a pour cible la page `changepassword.php`.
2. Assurez-vous que ce formulaire soit présenté uniquement si l'utilisateur est
connecté ET qu'il demande cette page avec un requête HTTP de type GET. Si ce
n'est pas le cas, redirigez-le vers `signin.php`.
3. Ajoutez un lien vers `formpassword.php` sur la page `welcome.php`, ainsi
qu'un lien vers `welcome.php` sur la page `formpassword.php`.
4. Créez un nouveau fichier `changepassword.php` qui effectue, dans l'ordre,
les traitement suivants :
    - il vérifie que l'utilisateur est connecté et accède à cette page via une
      requête HTTP de type POST. Si ce n'est pas le cas, il demande une
      redirection vers `formpassword.php`.
    - il vérifie et sécurise les données transmises en POST : si elles
      n'existent pas, il demande une redirection vers `formpassword.php`.
    - il vérifie que le mot de passe et sa confirmation sont identique : si ce
      n'est pas le cas, il demande une redirection vers `formpassword.php`.
    - si tout est bon jusque là, il change le mot de passe de l'utilisateur
      dans la BDD.
    - si la requête de changement de mot de passe se passe bien, il demande une
      redirection vers `welcome.php`. Sinon il demande une redirection vers
      `formpassword.php`.

Si vous gérez les messages d'erreur (voir Exercice 5 du TP2), pensez à mettre à
jour la variable de session `message` avant chaque demande de redirection dans
`changepassword.php`.


Exercice 2 : Suppression d'un utilisateur
-----------------------------------------

1. Ajouter un lien vers `deleteuser.php` dans le fichier `welcome.php`.
2. Créez un nouveau fichier `deleteuser.php` qui effectue, dans l'ordre,
les traitement suivants :
    - il vérifie que l'utilisateur est connecté et accède à cette page via une
      requête HTTP de type GET. Si ce n'est pas le cas, il demande une
      redirection vers `welcome.php`.
    - si tout est bon jusque là, il supprime l'utilisateur de la BDD
    - si la requête s'est bien passée, il supprime la session et demande une
      redirection vers `signin.php`. Sinon il demande une redirection vers
      `welcome.php`.

Si vous gérez les messages d'erreur (voir Exercice 5 du TP2), pensez à mettre à
jour la variable de session `message` avant chaque demande de redirection dans
`deleteuser.php`.


Exercice 3 : Passage en POO - Les bases
---------------------------------------

Cet exercice va vous permettre de créer une classe `User` faisant le lien avec
entre votre BDD et le reste de votre code.

1. Créez un répertoire `models` contenant deux nouveaux fichiers :
`model_base.php` et `user.php`.
2. Copiez-collez le contenu suivant dans `model_base.php` : ce sera la classe
mère de toutes les classes modèle.
```
<?php
class Model_Base
{
	protected static $_db;

	public static function set_db(PDO $db) {
		self::$_db = $db;
	}
}
```
Avant toute utilisation d'une classe fille de la classe `Model_Base`, il faudra
appeler la fonction statique `set_db`. Attention, cette instruction ne doit
intervenir qu'une fois par requête du client !

3. Dans `user.php`, écrivez la classe `User` qui étend la classe `Model_Base`.
Cette classe doit déclarer :
    - deux membres privés `$_login` et `$_password`
    - un membre privé constant USER_TABLE correspondant au nom de la table dans
      laquelle vous stockez vos utilisateurs
    - un constructeur prenant en paramètre un login et un mot de passe
    - un getter et un setter pour chacun des champs


Exercice 4 : Passage en POO - Authentification
----------------------------------------------

1. Dans la classe `User`, écrivez une méthode publique `exists`, sans
paramètre, qui réalise le travail du fichier `authenticate.php`, c'est-à-dire :
    - fait une requête vers la BDD pour récupérer l'utilisateur dont le login
      correspond au membre `$_login` de la classe
    - si la requête s'exécute bien et retourne au moins une ligne, vérifie que
      le mot de passe du membre `$_password` correspond à celui retourner par
      la première ligne de la requête
    - retourne un booléen qui vaut vrai si l'utilisateur existe avec le bon mot
      de passe, faux si quelque chose se passe mal.

2. Modifiez le fichier `authenticate.php` en remplaçant tout ce qui concerne
les requêtes vers la BDD par :
    - la création d'un objet `User` à partir des variables récupérées en POST
    - l'utilisation de la méthode `exists` pour vérifier si l'utilisateur
      existe dans la BDD.

   Les redirections et création de sessions restent identiques en fonction de
   l'existence ou non de l'utilisateur dans la BDD.


Exercice 5 : Passage en POO - Ajout d'un utilsateur
---------------------------------------------------

1. Dans la classe `User`, écrivez une méthode publique `create`, sans
paramètre, qui réalise le travail du fichier `adduser.php`, c'est-à-dire :
    - fait une requête vers la BDD pour insérer l'utilisateur `$this`
    - si la requête s'exécute bien, ne fait rien
    - si la requête échoue, déclenche une exception PHP
2. Modifiez le fichier `adduser.php` en remplaçant tout ce qui concerne
les requêtes vers la BDD par :
    - la création d'un objet `User` à partir des variables récupérées en POST
    - l'utilisation de la méthode `create` pour insérer l'utilisateur dans la
      BDD.

   Les redirections et création de sessions restent identiques et se font en
   fonction du déclenchement d'une exception ou non par la méthode `create`.


Exercice 6 : Passage en POO - Changement de mot de passe
--------------------------------------------------------

1. Dans la classe `User`, écrivez une méthode publique `changePassword`, avec
un mot de passe en paramètre, qui réalise le travail du fichier
`changepassword.php`, c'est-à-dire :
    - fait une requête vers la BDD pour mettre à jour le mot de passe de
      l'utilisateur `this` avec celui passé en paramètre
    - si la requête s'exécute bien, met à jour le membre `$_password` avec le
      nouveau mot de passe
    - si la requête échoue, déclenche une exception PHP
2. Modifiez le fichier `changepassword.php` en remplaçant tout ce qui concerne
les requêtes vers la BDD par :
    - la création d'un objet `User` à partir de son login
    - l'utilisation de la méthode `changePassword` pour modifier le mot de
      passe de l'utilisateur dans la BDD.

   Les redirections et création de sessions restent identiques et se font en
   fonction du déclenchement d'une exception ou non par la méthode
   `changePassword`.


Exercice 7 : Passage en POO - Suppression d'un utilisateur
----------------------------------------------------------

1. Dans la classe `User`, écrivez une méthode publique `delete`, avec
un mot de passe en paramètre, qui réalise le travail du fichier
`deleteuser.php`, c'est-à-dire :
    - fait une requête vers la BDD pour supprimer l'utilisateur `this`
    - si la requête s'exécute bien, ne fait rien
    - si la requête échoue, déclenche une exception PHP
2. Modifiez le fichier `deleteuser.php` en remplaçant tout ce qui concerne
les requêtes vers la BDD par :
    - la création d'un objet `User` à partir de son login
    - l'utilisation de la méthode `delete` pour supprimer l'utilisateur de la
      BDD.

Les redirections et création de sessions restent identiques et se font en
fonction du déclenchement d'une exception ou non par la méthode
`changePassword`.


Pour les plus rapides
---------------------

- Modifier l'architecture de votre application pour mettre les fichiers
contrôleur et vue dans des répertoires `controlers` et `views` au même niveau
que `models`.
- Mettez en place le style de toutes vos vues à l'aide de
[Materialize](https://materializecss.com/)
