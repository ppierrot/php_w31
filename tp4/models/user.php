<?php

  require_once('models/model_base.php');

  class user extends model_base{

    /*membres*/

    private $_login;
    private $_password;
    private const USER_TABLE = "Users";

    /*constructeur de la classe*/

    function __construct(string $login, string $password) { 
      
      $this->_login = $login; 
      $this->_password = $password; 
    
    }

    /*getters*/

    function getLogin(){ 
      return $this->_login; 
    }

    function getPaswword(){ 
      return $this->_password; 
    }

    /*setters*/

    function setLogin(string $log){ 
      $this->_login = $log; 
    }

    function setPassword(string $pass){ 
      $this->_password = $pass; 
    }

    /*méthodes*/

    /*vérifie si utilisateur existe lors de l'identification*/
    function exists() {

      //on prépare une requête sql 
      $result = model_base::$_db->prepare("SELECT login, password FROM " . user::USER_TABLE . " WHERE login = :login");

      //on lie les variables d'intégrité html à ceux utilisés dans la requête sql puis on éxécute la requête 
      $ok1 = $result->bindValue( ':login', $this->_login, PDO::PARAM_STR);
      $ok2 = $result->execute();

      //si la requête a bien été exécuté, ça veut dire que login et mot de passe sont correct et on renvoi vers welcome.php
      if ($ok2 == true){

        $row = $result->fetch(PDO::FETCH_ASSOC); // transforme résultat inexploitable de la requête en tableau 

        //vérification mot de passe déhaché = mot de passe entré dans signin
        if(password_verify($this->_password, $row['password'])){
          return true;
        }
        else{
          return false;
        }
      }
      else{
        return false;
      }
    }
  
    /*Créé un nouvel utilisateur dans la bdd*/
    public function create() {

      include 'bdd.php';

      model_base::set_db($db);

      $result = model_base::$_db->prepare("INSERT INTO " . user::USER_TABLE . " (login, password) values (:login, :password)");

      $ok1 = $result->bindValue( ':login', $this->_login, PDO::PARAM_STR); 
      $ok2 = $result->bindValue( ':password', password_hash($this->_password, PASSWORD_DEFAULT), PDO::PARAM_STR);
      
      try{
        $ok3 = $result->execute();
      } catch(Exception $e){
        echo $e->getMessage();
      }
    }

    /*change le mot de passe de l'utilisateur connécté*/
    public function changePassword($mdp){

      include 'bdd.php';

      model_base::set_db($db);

      $result = model_base::$_db->prepare("UPDATE " . user::USER_TABLE . " SET password = :mdp where login = :login");

      //on lie les variables locales à la requête SQL
      $ok1 = $result->bindValue(':mdp', password_hash($mdp, PASSWORD_DEFAULT), PDO::PARAM_STR);
      $ok2 = $result->bindValue(':login',$this->_login, PDO::PARAM_STR);
      
      try{
        $ok3 = $result->execute();
      } catch(Exception $e){
        echo $e->getMessage();
      }

      $this->_password = $mdp;  
    }

    /*supprimer le compte de l'utilisateur*/
    public function delete(){

      include 'bdd.php';

      model_base::set_db($db);

      //on prépare une requête sql 
      $result = model_base::$_db->prepare("DELETE FROM " . user::USER_TABLE . " WHERE login = :login");

      //on lie les variables locales à la requête SQL
      $ok1 = $result->bindValue(':login',$this->_login, PDO::PARAM_STR);
      
      try{
        $ok2 = $result->execute();
      } catch(Exception $e){
        echo $e->getMessage();
      }

    }

  }

?>