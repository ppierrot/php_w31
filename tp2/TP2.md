TP2 - Gestion de comptes utilisateur
====================================

Créez dans votre clone Git un répertoire `TP2` contenant les 5 fichiers vides
suivants :
- `signin.php`
- `authenticate.php`
- `welcome.php`
- `signout.php`
- `users.php`

Objectifs
----------

L'objectif des exercices suivants est de compléter ces 4 fichiers pour
mettre en place un mécanisme d'authentifications d'utilisateurs. Ils
serviront de base pour les TPs suivants.

Pour tous les exercices du TP, pensez bien à :
- vérifier et sécuriser les variables que l'utilisateur peut modifier
- penser à tous les cas de figures envisageables pour accéder à vos pages
- n'oubliez pas de commiter/pusher votre travail sur le dépôt Git !

Exercice 1 : Formulaire de connexion
------------------------------------

Complétez le fichier PHP `signin.php` qui renvoie un document HTML contenant
un formulaire proposant de s'authentifier à l'aide d'un identifiant et d'un
mot de passe.

Recommandations :
- choisissez des champs de formulaires appropriés aux éléments attendus
- assurez-vous que le formulaire ne soit proposé que si la requête HTTP utilise
la méthode GET.

Exercice 2 : Authentification
-----------------------------

Complétez le script PHP `authenticate.php` qui traite la requête HTTP POST du
formulaire `signin.php` comme suit :

1. il inclut le fichier `users.php` qui contient un tableau associant un nom
d'utilisateur à son mot de passe
2. il vérifie que le tableau contient le nom d'utilisateur entré dans le
formulaire existe et que le mot de passe associé est correct.
3. Si l'authentification est réussie :
    - une session est créée
    - le nom d'utilisateur est sauvegardé dans cette session
    - une demande de redirection vers le fichier `welcome.php` est envoyée
4. Si l'authentification échoue, une demande de redirection vers le fichier
`signin.php` est envoyée.

Exercice 3 : Mon compte
-----------------------

Complétez le fichier PHP `welcome.php` pour qu'il se comporte ainsi :

1. Si l'utilisateur ne s'est pas encore authentifié, une demande de redirection
vers le fichier `signin.php` est envoyée.
2. Si l'utilisateur est authentifié, ce fichier lui propose un contenu HTML
comportant un message de bienvenue personnalisé ainsi qu'un lien de déconnexion
vers le fichier PHP `signout.php`

Exercice 4 : Déconnexion
------------------------

Complétez le fichier PHP `signout.php` pour qu'il effectue les opérations
suivantes :

1. effacer l'identité de l'utilisateur connecté de la session courante
2. envoyer une demande de redirection vers le fichier `signin.php`

Exercice 5 : Messages d'erreur du serveur vers le client
--------------------------------------------------------

Cet exercice propose de mettre en place un système de messages affichés sur la
page `signin.php` pour indiquer à l'utilisateur le problème rencontré par le
module PHP.

1. Dans le fichier `sigin.php`, prévoyez une balise `div` avec pour class
`message` qui affiche le contenu de la variable de session `message` si elle
existe et n'est pas vide.
2. Dans le fichier `authenticate.php`, mettez à jour la variable de session
`message` en fonction des différents problèmes rencontrés lors de la
tentative d'authentification :
    - le nom d'utilisateur n'existe pas
    - le mot de passe est incorrect

Pour les plus rapides
---------------------

- Niveau 1 : Ajoutez un fichier de style CSS pour mettre en forme votre formulaire.
- Niveau 2 : Utilisez [Materialize](https://materializecss.com/) pour mettre en
forme vos différentes pages.
