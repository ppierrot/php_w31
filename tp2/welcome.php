<?php
/*Exercice 3 : Mon compte
-----------------------

Complétez le fichier PHP `welcome.php` pour qu'il se comporte ainsi :

1. Si l'utilisateur ne s'est pas encore authentifié, une demande de redirection
vers le fichier `signin.php` est envoyée.
2. Si l'utilisateur est authentifié, ce fichier lui propose un contenu HTML
comportant un message de bienvenue personnalisé ainsi qu'un lien de déconnexion
vers le fichier PHP `signout.php` */

  if(isset($_SESSION)){
    echo "Bienvenue, " . $_GET['login'] . ' !';
    echo '<a href="signout.php">Deconnexion</a>';
  }
  else{
    header('Location: signin.php');
    exit();
  }
?>