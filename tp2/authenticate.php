<?php

  if(isset($_GET['login'] && isset($_GET['password'])){

    include 'users.php'; //on peut aussi utiliser 'require', la diff c'est qu'il balancera une érreur si ya pas users.php

    //protège l'intégrité des balises html enregistrées
    $login = htmlentities($_GET['login']);
    $password = htmlentities($_GET['password']);

    if (array_key_exists($login, $users)){
      if (password_verify($password, $users[$login])){
        $_SESSION['login'] = $login;
        session_start();
        $_SESSION[] = $_GET['login'];
        header('Location: welcome.php');
        exit();
      } 
      else {
        header('Location: signin.php');
        exit();
      }
    }
    else{
      header('Location: signin.php');
      exit();
    }
  }

?>